# Tarefa Javascript III (TT 2022.1)

## Arquivos principais da tarefa
- LandingPage.html (dentro da pasta public)
- Cadastro.html (dentro da pasta public)
- cadastro.js (dentro da pasta controllers, a qual está dentro da pasta src)
- postCadastro.js (dentro da pasta controllers, a qual está dentro da pasta src)

## 💻 Rodando a tarefa na máquina
abra o html com Live Server (extensão baixável do vscode)
<br>

## 📓 Conteúdo da tarefa

- JavaScript - DOM
- HTTP protocol
- CRUD - HTTP methods
- Fetch API

<hr><br>

## 🛠 Tecnologias e ferramentas

- [JavaScript](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript)
- [Fetch API](https://developer.mozilla.org/pt-BR/docs/Web/API/Fetch_API)
- [JSON Server](https://www.npmjs.com/package/json-server)

<hr><br>


#### ➡️ Clone esse repositório

```
git clone https://gitlab.com/matheuspercine/formulario-civitas.git
```

#### ➡️ Entre na pasta do projeto

```
cd aula-js-dom
```

#### ➡️ (Opcional) Se preferir a abordagem funcional, continue na branch `main`. Caso prefira a abordagem POO (orientada a objetos), altere sua branch usando o comando

```
git checkout poo
```

#### ➡️ Abra o terminal (pode ser pelo VSCode) e execute o comando para instalar as dependências

```
npm install
```

#### ➡️ Execute o comando para iniciar a API

```
json-server --watch db.json
```

- Se esse comando não rodar, tente:

```
npx json-server --watch db.json
```

#### ➡️ Abra o arquivo `LandingPage.html` para iniciar a aplicação

<br>

#### ➡️ Para parar de servir a API, basta executar o comando `CTRL + C` no terminal

<hr>

<div align="center">
Copyright © 2022<br> 
  ✨ Created by <b>Matheus Percine</b> ✨
</div>
